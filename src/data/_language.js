export const french = {
    language: 'Français',
    navLinks: {
        about: 'Profil',
        skills: 'Compétences',
        experiences: 'Expériences',
        education: 'Formations',
        portfolio: 'Projets'
    }

}

export const english = {
    language: 'English',
    navLinks: {
        about: 'About me',
        skills: 'Skills',
        experiences: 'Experiences',
        education: 'Education',
        portfolio: 'Portfolio'
    }

}

