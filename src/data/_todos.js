import Todo from '../models/Todo'

const todos = [
    new Todo('faire le ménage', false),
    new Todo('faire à manger', false),
    new Todo('créer un virus et foutre en l\'air l\'économie', true),
    new Todo('acheter des épices à guacamole', true)
]
export default todos