export const hortensia = {
    name: 'hortensia',
    primary: {
        main: '#c5e1a5',
        light: '#f8ffd7',
        dark: '#94af76',
        color: '#262626',
    },
    secondary: {
        main: '#5c6bc0',
        light: '#8e99f3',
        dark: '#26418f',
        color: 'white',
    },
    light: '#e0e0e0',
    dark: 'black',
}

export const corail = {
    name: 'corail',
    primary: {
        main: '#ef5350',
        light: '#ff867c',
        dark: '#b61827',
        color: 'white',
    },
    secondary: {
        main: '#ff4349',
        light: '#ff7a75',
        dark: '#c40021',
        color: 'white',
    },
    light: '#e0e0e0',
    dark: 'black',
}

export const sunset = {
    name: 'sunset',
    primary: {
        main: '#ff5722',
        light: '#ff8a50',
        dark: '#c41c00',
        color: 'white',
    },
    secondary: {
        main: '#8c9eff',
        light: '#c0cfff',
        dark: '#5870cb',
        color: 'white',
    },
    light: '#e0e0e0',
    dark: 'black',
}