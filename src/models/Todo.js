class Todo {
    constructor(content, isCompleted) {
        this.content = content
        this.isCompleted = isCompleted
    }
}
export default Todo