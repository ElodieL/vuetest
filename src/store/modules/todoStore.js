import todos from '../../data/_todos'

const state = {
    todos: todos
}
const getters = {
    allTodos: state => state.todos,
    completedTodos: state => state.todos.filter(todo => todo.isCompleted),
    remainingTodos: state => state.todos.filter(todo => !todo.isCompleted),
    completedTodosCount: state => getters.completedTodos(state).length,
    remainingTodosCount: state => getters.remainingTodos(state).length,
}
const mutations = {
    ADD_TODO: (state, content) => {
        state.todos.push({
            content: content,
            isCompleted: false
        })
    }
}
const actions = {
    addTodo: (store, content) => {
        store.commit('ADD_TODO', content)
    }
}

const todoStore = {
    state,
    getters,
    mutations,
    actions
}
export default todoStore