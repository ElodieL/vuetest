import { french, english } from '../../data/_language'

const state = {
    languages: {
        french: french,
        english: english,
    },
    currentLanguage: french,
}

const getters = {
    french: state => state.languages.french,
    english: state => state.languages.english,
    currentLanguage: state => state.currentLanguage,
    currentNavLinks: state => state.currentLanguage.navLinks,

}

const mutations = {
    SET_CURRENT_LANGUAGE: (state, language) => {
        state.currentLanguage = language
    }
}

const actions = {
    setCurrentLanguage: (store, language ) => {
        store.commit('SET_CURRENT_LANGUAGE', language)
    }
}

const languageStore = {
    state,
    getters,
    mutations,
    actions
}

export default languageStore