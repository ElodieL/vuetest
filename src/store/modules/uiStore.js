const state = {
    drawerState: false
}

const getters = {
    drawerState: state => state.drawerState
}

const mutations = {
    OPEN_DRAWER: (state) => {
        state.drawerState = true;
    },
    CLOSE_DRAWER: (state) => {
        state.drawerState = false;
    }
}

const actions = {
    openCloseDrawer: (store, drawerState) => {
        if (drawerState === true) {
            store.commit('CLOSE_DRAWER')
        } else if(drawerState === false) {
            store.commit('OPEN_DRAWER')
        }
    }
}

const uiStore = {
    state,
    getters,
    mutations,
    actions
}
export default uiStore