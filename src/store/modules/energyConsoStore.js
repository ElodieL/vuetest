import Vue from "vue"
import Vuex from "vuex"
import axios from "axios"
import VueAxios from "vue-axios"

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = 'https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/'

const state = {
    electricityMeter: {},
    gasMeter: {},
    electricity: {},
    gas: {}
}

const getters = {
    electricityMeter: state => state.electricityMeter,
    gasMeter: state => state.gasMeter,
    electricity: state => state.electricity,
    gas: state => state.gas
}

const mutations = {
    SAVE_ELECTRICITY_METER: (state, meter) => {
        state.electricityMeter = meter
    },
    SAVE_GAS_METER: (state, meter) => {
        state.gasMeter = meter
    },
    SAVE_ELECTRICITY: (state, electricity) => {
        state.electricity = electricity
    },
    SAVE_GAS: (state, gas) => {
        state.gas = gas
    }
}

const actions = {
    async loadElectricityMeter({commit}) {
        await Vue.axios.get('meter/2').then(result => { 
            commit('SAVE_ELECTRICITY_METER', result.data)
            console.log(result) 
        }).catch(error => { 
            throw new Error(`API ${error}`)
        })
    },
    async loadGasMeter({commit}) {
        await Vue.axios.get('meter/1').then(result => { 
            commit('SAVE_GAS_METER', result.data)
            console.log(result) 
        }).catch(error => { 
            throw new Error(`API ${error}`)
        })
    },
    async loadElectricity({commit}) {
        await Vue.axios.get('meter/2/electricity').then(result => { 
            commit('SAVE_ELECTRICITY', result.data)
            console.log(result) 
        }).catch(error => { 
            throw new Error(`API ${error}`)
        })
    },
    async loadGas({commit}) {
        await Vue.axios.get('meter/1/gas').then(result => { 
            commit('SAVE_GAS', result.data)
            console.log(result) 
        }).catch(error => { 
            throw new Error(`API ${error}`)
        })
    }
}

const energyConsoStore = {
    state,
    getters,
    mutations,
    actions
}
export default energyConsoStore



