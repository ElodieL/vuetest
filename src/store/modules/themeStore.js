import { hortensia, corail, sunset } from '../../assets/themes'

const state = {
    themes: {
        hortensia, 
        corail, 
        sunset
    },
    currentTheme: hortensia
}

const getters = {
    allThemes: state => state.themes,
    currentTheme: state => state.currentTheme,
    hortensia: state => state.themes.hortensia,
    corail: state => state.themes.corail,
    sunset: state => state.themes.sunset,
}

const mutations = {
    SET_CURRENT_THEME: (state, theme) => {
        state.currentTheme = theme
    }
}

const actions = {
    setCurrentTheme: (store, theme) => {
        store.commit('SET_CURRENT_THEME', theme)
    }
}

const themeStore = {
    state,
    getters,
    mutations,
    actions
}
export default themeStore