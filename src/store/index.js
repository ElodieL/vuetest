import Vue from 'vue'
import Vuex from 'vuex'
//import todoStore from './modules/todoStore'
import themeStore from './modules/themeStore'
import uiStore from './modules/uiStore'
import energyConsoStore from './modules/energyConsoStore'
import languageStore from './modules/languageStore'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    themeStore,
    uiStore,
    energyConsoStore,
    languageStore,
  },
  strict: true,
  plugins: []
})

global.store = store
export default store