import styled, { css } from 'vue-styled-components'


const propsContainer = { 
    currentTheme: Object, 
    primary: Boolean,
    secondary: Boolean,
    appbar: Boolean, 
    drawer: Boolean,
    flex: Boolean,
    column: Boolean,
    alignCenter: Boolean
}

const Container = styled('div', propsContainer)`
    ${ props => props.appbar && css`
        z-index: 2;
        position: -webkit-sticky;
        position: sticky;
        top: 0px;
        z-index: 1;
        display: flex;
        justify-content: space-between;
        align-items: center;
        min-width: 100vw;
        height: 65px;
        padding: 10px;
        background-color: white;
        background-color: ${ props => props.primary && props.currentTheme.primary.main };
        background-color: ${ props => props.secondary && props.currentTheme.secondary.main };
        -webkit-box-shadow: -1px 4px 14px 3px rgba(0,0,0,0.3);
        -moz-box-shadow: -1px 4px 14px 3px rgba(0,0,0,0.3);
        box-shadow: -1px 4px 14px 3px rgba(0,0,0,0.3);
        div {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        h1 {
            color: white;
            font-size: 1.5rem; 
        }
        ul {
            display: flex;
            flex-direction: row;
            li {
                margin: 10px;
                color: white;
                &:hover {
                    color: ${ props => props.primary ? props.currentTheme.secondary.dark : props.currentTheme.primary.dark };
                }
            }
        }
    `}

    ${ props => props.drawer && css`
        z-index: 0;
        position: fixed;
        width: 15%;
        min-height: 100vh;
        padding: 15px;
        display: flex;
        flex-direction: column;
        background-color: ${ props => props.primary && props.currentTheme.primary.dark };
        background-color: ${ props => props.secondary && props.currentTheme.secondary.dark };
        -webkit-box-shadow: 5px 3px 8px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 5px 3px 8px 0px rgba(0,0,0,0.16);
        box-shadow: 5px 3px 8px 0px rgba(0,0,0,0.16);
        color: white;
    `}
 
    ${ props => props.flex && css`
        display: flex;
        ${props => props.column && css`
            flex-direction: column;
        `}
        ${props => props.alignCenter && css`
            align-items: center;
        `}
    `}
`

export default Container