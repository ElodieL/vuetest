import styled, { css } from 'vue-styled-components'

const propsButton = {
    currentTheme: Object,
    primary: Boolean,
    secondary: Boolean,
    rounded: Boolean,
    iconButton: Boolean,
}

const Button = styled('button', propsButton)`
    background-color: white;
    outline: inherit;
    margin: 5px;
    border: 2px solid ${ props => props.primary && props.currentTheme.primary.light };
    border: 2px solid ${ props => props.secondary && props.currentTheme.secondary.light };
    
    ${ props => props.rounded && css`
        border-radius: 100%;
        padding: 15px;
    `}
    background-color: ${ props => props.primary && props.currentTheme.primary.dark };
    background-color: ${ props => props.secondary && props.currentTheme.secondary.dark };
    
    ${ props => props.iconButton && css`
        background: none;
        color: inherit;
        border: none;
        padding: 10px;
        font: inherit;
        cursor: pointer;
        
    `}

`
export default Button;